use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;
use std::time::{Duration, Instant};
use std::f64::consts::{
    PI,
    FRAC_PI_3,
};

use anyhow::{anyhow, Result};

const LIMIT: i64 = 200;

#[derive(Debug, Clone, Copy, Default)]
struct Vector2 {
    x: f64,
    y: f64,
}

trait SetFn {
    fn contained(&self) -> Option<f64>;
}

impl Vector2 {
    pub fn new<T: Into<f64>>(x: T, y: T) -> Vector2 {
        Vector2 {
            x: x.into(),
            y: y.into(),
        }
    }

    #[inline]
    pub fn length_sq(&self) -> f64 {
        self.x * self.x + self.y * self.y
    }

    #[inline]
    pub fn square_assign(&mut self) {
        *self = Vector2 {
            x: self.x * self.x - self.y * self.y,
            y: 2.0 * self.x * self.y,
        }
    }

    #[inline]
    pub fn point_mul_assign(&mut self, other: Vector2) {
        *self = Vector2 {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }

    #[inline]
    pub fn point_div_assign(&mut self, other: Vector2) {
        *self = Vector2 {
            x: self.x / other.x,
            y: self.y / other.y,
        }
    }
}

impl std::ops::Add for Vector2 {
    type Output = Vector2;
    fn add(self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::AddAssign for Vector2 {
    fn add_assign(&mut self, other: Vector2) {
        *self = Vector2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl std::ops::Sub for Vector2 {
    type Output = Vector2;

    fn sub(self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl SetFn for Vector2 {
    fn contained(&self) -> Option<f64> {
        let mut point_iter = Vector2::default();
        let mut i = 0;

        while point_iter.length_sq() <= 4.0 && i < LIMIT {
            point_iter.square_assign();
            point_iter += *self;

            i += 1;
        }

        if point_iter.length_sq() <= 4.0 {
            None
        } else {
            Some(i as f64 / LIMIT as f64)
        }
    }
}

impl From<Vector2> for Point {
    fn from(v: Vector2) -> Self {
        Point::new(v.x as i32, v.y as i32)
    }
}

#[derive(Debug, Default)]
struct Rect {
    origin: Vector2,
    size: Vector2,
}

impl Rect {
    fn new<T: Into<f64>>(x: T, y: T, sx: T, sy: T) -> Rect {
        Rect {
            origin: Vector2::new(x.into(), y.into()),
            size: Vector2::new(sx.into(), sy.into()),
        }
    }

    fn from_start_end(origin: Vector2, end: Vector2) -> Rect {
        let size = end - origin;
        Rect { origin, size }
    }
}

struct RectMapping<'a> {
    source: &'a Rect,
    dest: &'a Rect,
}

impl<'a> RectMapping<'a> {
    fn new(source: &'a Rect, dest: &'a Rect) -> RectMapping<'a> {
        RectMapping { source, dest }
    }

    fn map(&self, v: &Vector2) -> Vector2 {
        let mut result = *v - self.source.origin;

        result.point_div_assign(self.source.size);

        result.point_mul_assign(self.dest.size);

        result += self.dest.origin;

        result
    }

    fn map_assign(&self, v: &mut Vector2) {
        *v = self.map(v);
    }
}

fn main() -> Result<()> {
    let sdl_context = sdl2::init().map_err(|e| anyhow!("{}", e))?;
    let video_subsystem = sdl_context.video().map_err(|e| anyhow!("{}", e))?;

    let window = video_subsystem
        .window("mandel", 800, 600)
        .position_centered()
        .build()?;

    let mut canvas = window.into_canvas().build()?;

    let texture_creator = canvas.texture_creator();

    let mut texture = texture_creator.create_texture_target(None, 800, 600)?;

    let viewport = Rect::new(0, 0, 800, 600);

    let mut camera = Rect::new(-2.5, -2.5, 5.0, 5.0);

    let mut event_pump = sdl_context.event_pump().map_err(|e| anyhow!("{}", e))?;

    let mut mouse_click: Option<Vector2> = None;
    let mut mouse_drag = Vector2::default();

    let mut redraw = true;

    let mut angle_offset = 0.7;

    'running: loop {
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();

        if redraw {

            println!("drawing for region: {:?}, with color offset {}", &camera, angle_offset);

            canvas.with_texture_canvas(&mut texture, |texture_canvas| {
                texture_canvas.set_draw_color(Color::BLACK);
                texture_canvas.clear();

                let mapping = RectMapping::new(&viewport, &camera);
                let mut camera_p: Vector2;

                let start = Instant::now();

                for y in 0..viewport.size.y as u32 {
                    for x in 0..viewport.size.x as u32 {
                        camera_p = Vector2::new(x, y);
                        let world_p = mapping.map(&camera_p);

                        if let Some(f) = world_p.contained() {
                            let angle = (f * 2.0 * PI) + ((angle_offset / 1.0) * 2.0 * PI);

                            let r_shade = (angle.sin() * 255.0) as u8;
                            let g_shade =
                                ((angle + (2.0 * FRAC_PI_3)).sin() * 255.0) as u8;
                            let b_shade =
                                ((angle + (4.0 * FRAC_PI_3)).sin() * 255.0) as u8;

                            texture_canvas.set_draw_color(Color::RGB(r_shade, g_shade, b_shade));
                        } else {
                            texture_canvas.set_draw_color(Color::BLACK);
                        }

                        texture_canvas.draw_point(camera_p).unwrap();
                    }
                }

                println!("took {}s", start.elapsed().as_secs_f32());
            })?;

            redraw = false;
        }

        canvas
            .copy(&texture, None, None)
            .map_err(|e| anyhow!("{}", e))?;

        if let Some(origin) = mouse_click {
            let size = mouse_drag - origin;
            canvas.set_draw_color(Color::WHITE);
            canvas
                .draw_rect(sdl2::rect::Rect::new(
                    origin.x as i32,
                    origin.y as i32,
                    size.x as u32,
                    size.y as u32,
                ))
                .map_err(|e| anyhow!("{}", e))?;
        }

        canvas.present();

        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => break 'running,

                Event::KeyDown {
                    keycode: Some(Keycode::R),
                    ..
                } => {
                    camera = Rect::new(-2.5, -2.5, 5.0, 5.0);
                    redraw = true;
                }
                Event::KeyDown {
                    keycode: Some(Keycode::W),
                    ..
                } => {
                    if !redraw {
                        angle_offset += 0.025;
                        redraw = true;
                    }
                }
                Event::KeyDown {
                    keycode: Some(Keycode::S),
                    ..
                } => {
                    if !redraw {
                        angle_offset -= 0.025;
                        redraw = true;
                    }
                }
                Event::MouseButtonDown { x, y, .. } => {
                    mouse_click = Some(Vector2::new(x, y));
                    mouse_drag = Vector2::new(x, y);
                }
                Event::MouseMotion { x, y, .. } => {
                    if let Some(_) = mouse_click {
                        mouse_drag = Vector2::new(x, y);
                    }
                }
                Event::MouseButtonUp { x, y, .. } => {
                    let mut mouse_up = Vector2::new(x, y);
                    let mut mouse_down = mouse_click.take().unwrap();

                    let mapping = RectMapping::new(&viewport, &camera);

                    mapping.map_assign(&mut mouse_up);
                    mapping.map_assign(&mut mouse_down);

                    camera = Rect::from_start_end(mouse_down, mouse_up);

                    redraw = true;
                }
                _ => {}
            }
        }
        ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
    }

    Ok(())
}
